Name:           ortp
Version:        4.5.15
Release:        2%{?dist}
Summary:        C library implementing the RTP protocol (RFC3550)
Epoch:          2

License:        GPLv3
URL:            https://gitlab.linphone.org/BC/public/ortp
Source0:        https://gitlab.linphone.org/BC/public/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz
Patch0:         %{name}-rpath.patch

BuildRequires:  bctoolbox-devel
BuildRequires:  cmake
BuildRequires:  doxygen
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  graphviz

%description
oRTP is a C library that implements RTP protocol (RFC3550).


%package        devel
Summary:        Development libraries for ortp
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    devel
Libraries and headers required to develop software with ortp.


%prep
%setup -q
%patch -p1 -b .rpath

%{__perl} -pi.dot  -e 's/^(HAVE_DOT\s+=)\s+NO$/\1 YES/;s/^(CALL_GRAPH\s+=)\s+NO$/\1 YES/;s/^(CALLER_GRAPH\s+=)\s+NO$/\1 YES/' ortp.doxygen.in


%build
%cmake -DENABLE_STATIC=OFF
%cmake_build


%install
%cmake_install
rm -r %{buildroot}%{_datadir}/doc/%{name}-*
mkdir -p %{buildroot}%{_mandir}/man3
install -p -m 0644 %{_vpath_builddir}/doc/man/man3/* %{buildroot}%{_mandir}/man3/

%ldconfig_scriptlets

%files
%doc AUTHORS.md CHANGELOG.md README.md
%license LICENSE.txt
%{_libdir}/libortp.so.15*

%files devel
%doc %{_vpath_builddir}/doc/html
%{_includedir}/%{name}
%{_libdir}/cmake/%{name}
%{_libdir}/libortp.so
%{_libdir}/pkgconfig/ortp.pc
%{_mandir}/man3/*.3*


%changelog
* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 2:4.5.15-2
- Rewrite spec to cmake, update to 4.5.15
